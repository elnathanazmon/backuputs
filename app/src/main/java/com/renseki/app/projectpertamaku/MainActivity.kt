package com.renseki.app.projectpertamaku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.main_activity_four.*
import kotlinx.android.synthetic.main.main_activity_three.*
import kotlinx.android.synthetic.main.main_activity_two.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initFive()
    }

    private fun initFive() {
       val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }

    private fun initFour() {
        setContentView(R.layout.main_activity_four)

        add.setOnClickListener {
            val strFirst = ed_first.text.toString()
            val strSecond = ed_second.text.toString()

            val intFirst = strFirst.toInt()
            val intSecond = strSecond.toInt()

            val res = intFirst + intSecond
            tv_result.text = "Hasilnya adalah $res"
        }
    }

    private fun String.debugToast(context: Context) {
        if (BuildConfig.DEBUG) {
            Toast
                    .makeText(context, this, Toast.LENGTH_LONG)
                    .show()
        }
    }

    private fun initThree() {
        setContentView(R.layout.main_activity_three)

        click_me.setOnClickListener {
            getString(R.string.sesuatu).debugToast(this)
        }
    }

    private fun initTwo() {
        setContentView(R.layout.main_activity_two)

        val strFirst = first.text.toString()
        val strSecond = second.text.toString()

        val intFirst = strFirst.toInt()
        val intSecond = strSecond.toInt()

        val res = intFirst + intSecond
        result.text = "Hasilnya adalah $res"
    }
}
